
public class AgentTypeOne {

	private double[][] qValues; // rows play B, columns play A
	private AgentRole role; // A or B
	private double temperature; // temperature boltzmann
	private double alpha; // learning rate
	private int[] C;
	private double initPr;
	private boolean firstRun;
	private double[] probabilities;

	public AgentTypeOne(AgentRole role, double temperature, double alpha, int actionsCount) {
		this.qValues = new double[actionsCount][actionsCount];
		this.role = role;
		this.temperature = temperature;
		this.alpha = alpha;
		this.C = new int[actionsCount];
		this.initPr = 1 / actionsCount;
		this.firstRun = true;
		this.probabilities = new double[actionsCount];
	}

	private int boltzmann(double temperature) { // returns index of the play
		//double den = 0;
		double sum = 0;
		for (int i = 0; i < qValues.length; i++) {
			for (int j = 0; j < probabilities.length; j++) {
				sum += Math.exp((qValues[i][j] * probabilities[i]) / temperature);
			}
		}
		double[] probs = new double[probabilities.length];
		for (int i = 0; i < qValues.length; i++) {
			for (int j = 0; j < probabilities.length; j++) {
				probs[i] = (Math.exp(  (qValues[i][j] * probabilities[i]) / temperature)) / sum;
			}

		}
//        for (int i = 0; i < qValues.length; i++) {
//            for (int j = 0; j < qValues[i].length; j++) {
//                den += Math.exp(qValues[i][j] / temperature);
//            }
//        }
		int index = 0;
		double minp = 0;
		double maxp = 0;
		double qp = (double) Math.random();
		for (int i = 0; i < probs.length; i++) {
			if (probs[i] != 0) {
				maxp += probs[i];
				if ((maxp >= qp) && (qp > minp)) {
					index = i;
					break;
				}
				minp = maxp;
			}
		}
		return index;
	}

	public int play() {
		return boltzmann(temperature);
	}

	public void updateQValues(int playA, int playB, int reward) {
		qValues[playB][playA] = (1 - alpha) * qValues[playB][playA] + alpha * reward;
	}

	private int g() {
		return 0;
	}

	public double[][] getqValues() {
		return qValues;
	}

	public void setqValues(double[][] qValues) {
		this.qValues = qValues;
	}

	public AgentRole getRole() {
		return role;
	}

	public void setRole(AgentRole role) {
		this.role = role;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public int[] getC() {
		return C;
	}

	public void setC(int[] C) {
		this.C = C;
	}

	public double getInitPr() {
		return initPr;
	}

	public void setInitPr(double initPr) {
		this.initPr = initPr;
	}

	public boolean isFirstRun() {
		return firstRun;
	}

	public void setFirstRun(boolean firstRun) {
		this.firstRun = firstRun;
	}

	public double[] getProbabilities() {
		return probabilities;
	}

	public void setProbabilities(double[] probabilities) {
		this.probabilities = probabilities;
	}

}
