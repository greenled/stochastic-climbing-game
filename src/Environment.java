public class Environment {

	private Point[][] rewards; // rows play B, columns play A
	private int[] Ca;
	private int[] Cb;
	private int totalB, totalA = 0;
	private double[][] Qvalues;
	private double rho;
	private double[] rPerEpisodeNB;
	private double[] rPerEpisodeCB;
	private int[][] count_time_par;
	private int actionsNumber;

	public Environment(Point[][] rewards) {
		this.rewards = rewards;

		actionsNumber = rewards.length;

		Ca = new int[actionsNumber];
		Cb = new int[actionsNumber];
		Qvalues = new double[actionsNumber][actionsNumber];
		this.rho = 0.5;
		count_time_par = new int[actionsNumber][actionsNumber];
	}

	private int getReward(int row, int column) {
		double randomNumber = Math.random();
		if (randomNumber < 0.5) {
			return rewards[row][column].getX();
		} else {
			return rewards[row][column].getY();
		}
	}
	// Boltzmann
	public Report train(int iterations) {
		Report report = new Report(iterations);
		AgentTypeOne agentOneA = new AgentTypeOne(AgentRole.A, 0.995, .1, actionsNumber);
		AgentTypeOne agentOneB = new AgentTypeOne(AgentRole.B, 10.995, .1, actionsNumber);
		rPerEpisodeNB = new double[iterations];
		int sum = 0;
		for (int i = 0; i < iterations; i++) {
			for (int j = 0; j < actionsNumber; j++) {
				totalA += Ca[j];
				totalB += Cb[j];
			}
			for (int j = 0; j < actionsNumber; j++) {
				for (int k = 0; k < actionsNumber; k++) {
					if (i == 0) {
						agentOneA.getProbabilities()[j] += 1 / actionsNumber;
						agentOneB.getProbabilities()[j] += 1 / actionsNumber;
					} else {
						agentOneA.getProbabilities()[j] += agentOneA.getqValues()[j][k] * (Cb[k] / totalB);
						agentOneB.getProbabilities()[j] += agentOneB.getqValues()[j][k] * (Ca[k] / totalA);
					}
				}

			}
			int playA = agentOneA.play();
			int playB = agentOneB.play();
			//Update Q-values matrix
			int reward = getReward(playA, playB);
			sum += reward;
			rPerEpisodeNB[i] = (double) sum / (i + 1);
			agentOneA.getqValues()[playA][playB] = (1 - agentOneA.getAlpha()) * agentOneA.getqValues()[playA][playB] + agentOneA.getAlpha() * reward;
			agentOneB.getqValues()[playA][playB] = (1 - agentOneB.getAlpha()) * agentOneB.getqValues()[playA][playB] + agentOneB.getAlpha() * reward;
			Ca[playB] += 1;
			Cb[playA] += 1;
			count_time_par[playA][playB] += 1;
			report.rewardsFirst[i] = rPerEpisodeNB[i];
//            agentOneA.updateQValues(playA, playB, reward);
//            agentOneB.updateQValues(playA, playB, reward);
		}

		return report;
	}
	// Combines Boltzmann 
	public Report trainCombined(int iterations) {
		Report report = new Report(iterations);
		double[] maxQA = new double[actionsNumber];
		double[] maxQB = new double[actionsNumber];
		int sum = 0;
		AgentTypeOne agentOneA = new AgentTypeOne(AgentRole.A, 0.995, .1, actionsNumber);
		AgentTypeOne agentOneB = new AgentTypeOne(AgentRole.B, 10.995, .1, actionsNumber);
		rPerEpisodeCB = new double[iterations];
		for (int i = 0; i < iterations; i++) {
			for (int j = 0; j < actionsNumber; j++) {
				totalA += Ca[j];
				totalB += Cb[j];
			}
			for (int j = 0; j < actionsNumber; j++) {
				for (int k = 0; k < actionsNumber; k++) {
					if (i == 0) {
						agentOneA.getProbabilities()[j] += 1 / actionsNumber;
						agentOneB.getProbabilities()[j] += 1 / actionsNumber;
					} else {
						agentOneA.getProbabilities()[j] += agentOneA.getqValues()[j][k] * (Cb[k] / totalB);
						agentOneB.getProbabilities()[j] += agentOneB.getqValues()[j][k] * (Ca[k] / totalA);
					}
				}

			}

			maxQA[0] = agentOneA.getqValues()[0][0];
			maxQA[1] = agentOneA.getqValues()[1][0];
			maxQA[2] = agentOneA.getqValues()[2][0];
			for (int k = 0; k < agentOneA.getqValues().length; k++) {
				for (int j = 0; j < agentOneA.getqValues()[0].length; j++) {
					if (maxQA[k] < agentOneA.getqValues()[k][j]) {
						maxQA[k] = agentOneA.getqValues()[k][j];
					}
				}
			}
			for (int k = 0; k < agentOneA.getqValues().length; k++) {
				agentOneA.getProbabilities()[k] = rho * maxQA[k] + (1.0 - rho) * agentOneA.getProbabilities()[k];
			}
			
			maxQB[0] = agentOneB.getqValues()[0][0];
			maxQB[1] = agentOneB.getqValues()[0][1];
			maxQB[2] = agentOneB.getqValues()[0][2];
			for (int k = 0; k < agentOneB.getqValues().length; k++) {
				for (int j = 0; j < agentOneB.getqValues()[0].length; j++) {
					if (maxQB[k] < agentOneB.getqValues()[k][j]) {
						maxQB[k] = agentOneB.getqValues()[k][j];
					}
				}
			}
			for (int k = 0; k < agentOneB.getqValues().length; k++) {
				agentOneB.getProbabilities()[k] = rho * maxQB[k] + (1.0 - rho) * agentOneB.getProbabilities()[k];
			}

			int playA = agentOneA.play();
			int playB = agentOneB.play();
			//Update Q-values matrix
			int reward = getReward(playA, playB);
			sum += reward;
			rPerEpisodeCB[i] = (double) sum / (i + 1);
			agentOneA.getqValues()[playA][playB] = (double) agentOneA.getqValues()[playA][playB] + ((1.0 / (count_time_par[playA][playB] + 1.0)) * (reward) - agentOneA.getqValues()[playA][playB]);
			agentOneB.getqValues()[playA][playB] = (double) agentOneB.getqValues()[playA][playB] + ((1.0 / (count_time_par[playA][playB] + 1.0)) * (reward) - agentOneB.getqValues()[playA][playB]);
			Ca[playB] += 1;
			Cb[playA] += 1;
			count_time_par[playA][playB] += 1;
			report.rewardsSecond[i] = rPerEpisodeCB[i];
		}

		return report;
	}

	public static class Report {

		private double[] rewardsFirst;
		private double[] rewardsSecond;

		public Report (int iterations) {
			rewardsFirst = new double[iterations];
			rewardsSecond = new double[iterations];
		}

		public double[] getRewardsFirst() {
			return rewardsFirst;
		}

		public double[] getRewardsSecond() {
			return rewardsSecond;
		}
	}
}
