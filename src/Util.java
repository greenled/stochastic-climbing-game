import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Util {
    /**
     * Write text to external file in a data directory
     * @param content
     * @param path
     */
    public static void writeToDataOutput(String content, Path path) {
        try {
            Files.write(path, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String arrayToLines (double[] array) {
        String lines = "";
        for (double a : array) {
            lines += a + "\n";
        }
        return lines;
    }
}
