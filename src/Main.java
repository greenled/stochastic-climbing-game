import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {
		String dataOutputDir = args[0];

		// rows(b) vs columns(a)
		Point[][] rewards = new Point[3][3];

		rewards[0][0] = new Point(10, 12);
		rewards[0][1] = new Point(5, -65);
		rewards[0][2] = new Point(8, -8);
		rewards[1][0] = new Point(5, -65);
		rewards[1][1] = new Point(14, 0);
		rewards[1][2] = new Point(12, 0);
		rewards[2][0] = new Point(5, -5);
		rewards[2][1] = new Point(5, -5);
		rewards[2][2] = new Point(10, 0);

		Environment env = new Environment(rewards);
		Environment.Report report = env.train(5000);
		Environment.Report report1 = env.trainCombined(5000);

		Path dataOutputDirectory = Paths.get(dataOutputDir);

		Util.writeToDataOutput(Util.arrayToLines(report.getRewardsFirst()), dataOutputDirectory.resolve("rewardsFirst.csv"));
		Util.writeToDataOutput(Util.arrayToLines(report1.getRewardsSecond()), dataOutputDirectory.resolve("rewardsSecond.csv"));
	}
}
