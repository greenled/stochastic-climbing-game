clc
clear all
close all

A= load('data/rewardsFirst.csv');
B= load('data/rewardsSecond.csv');

figure
hold on
plot(A,'b');
plot(B,'r');
legend('Boltzman','Combined Boltzman');
axis([0 5000 -5 15]);
title('Collected Rewards');
xlabel('Iterations');
ylabel('Average');
print('eps/0','-depsc');
print('png/0','-dpng');
